package com.jp.trc.trts.controller;

import com.jp.trc.trts.model.exam.Database;
import com.jp.trc.trts.model.exam.Exam;
import com.jp.trc.trts.model.user.User;
import com.jp.trc.trts.model.user.UserType;

import java.util.List;
import java.util.Optional;

/**
 * There are creates Database class object and search users in the database.
 */
public class UserController {

    private Database database;

    /**
     * Creates object of database class.
     * @param database current database.
     */
    public UserController(Database database) {
        this.database = database;
    }

    /**
     * This method searches for user by the entered username in Database arrays and return user group ID.
     *
     * @return userId return value user group, witch belong current username or null if user is not exist.
     */
    public UserType searchUserGroupByUserName(String currentName) {

        Optional<User> user = database.getUser().stream().filter(x -> x.getUserName().equals(currentName)).findFirst();
        return user.isPresent() ? user.get().getUserType(): null;
    }

    /**
     *Gets exam from list.
     * @return current exam.
     */
    public List<Exam> getTests() {
        return this.database.getExamList();
    }
    /**
     *Gets user from list.
     * @return current user.
     */
    public List<User> getUser() {
        return this.database.getUser();
    }

}
