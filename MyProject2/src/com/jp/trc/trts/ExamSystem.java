package com.jp.trc.trts;

/*
  @author Timirev
 * @version 1.0
 */

import com.jp.trc.trts.model.exam.Database;
import com.jp.trc.trts.controller.UserController;
import com.jp.trc.trts.view.InputService;
import com.jp.trc.trts.view.OutputService;
import com.jp.trc.trts.view.ConsoleUI;

/**
 * This is where the creation and initialization of database objects.
 */
public class ExamSystem {

    /**
     * Here start point of the program.
     */
    public static void main(String[] args) {

        /**
         * Adds users and tests into in memory database now.
         */
        Database database = new Database();
        database.init();

        UserController userController = new UserController(database);

        OutputService outputService = new OutputService();
        InputService inputService = new InputService();

        ConsoleUI consoleUI = new ConsoleUI(userController, outputService, inputService);
        consoleUI.run();
    }
}













