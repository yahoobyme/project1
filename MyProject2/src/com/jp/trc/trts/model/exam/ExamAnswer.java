package com.jp.trc.trts.model.exam;

/**
 *  There are creates answers for exam questions.
 */
public class ExamAnswer {
    private  int id;
    private String answerContent;
    private int questionId;
    boolean correct;

    /**
     * Creates objects answers of questions.
     * @param id ID answer.
     * @param answerContent content of answer.
     * @param questionId ID question.
     * @param correct correct answer for question.
     */
    public ExamAnswer (int id, String answerContent, int questionId, boolean correct){
        this.id = id;
        this.answerContent =answerContent;
        this.questionId =questionId;
        this.correct = correct;
    }
}
