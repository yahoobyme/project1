package com.jp.trc.trts.model.exam;

/**
 * There are creates and fill main array of questions with answers and right answers.
 * Test question contains answers and correct answer ID.
 */
public class ExamQuestion {

    private final int questionId;
    private final String questionText;


    /**
     * Gets question ID.
     *
     * @return current question ID.
     */

    public int getQuestionId() {
        return questionId;
    }

    /**
     * Gets question text.
     *
     * @return current question text.
     */
    public String getQuestionText() {
        return questionText;
    }
    /**
     * Gets question correct answer.
     * @return current correct answer.
     */


    /**
     * Creates TestQuestion class object.
     *
     * @param questionId   ID of question in test.
     * @param questionText text of question.
     */
    public ExamQuestion(int questionId, String questionText) {

        this.questionId = questionId;
        this.questionText = questionText;

    }
}

