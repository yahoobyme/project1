package com.jp.trc.trts.model.exam;

import java.util.Date;
import java.util.Hashtable;

/**
 * Collection of answers given by a given student on one exam subject
 */
public class ExamAssignment {

    private ExamSubject examSubject;

    private Date dateTaken;

    private Hashtable<Integer, Integer> answers;

    /**
     * Gets exam subject.
     * @return current exam subject.
     */
    public ExamSubject getExamSubject() {
        return examSubject;
    }

    /**
     * Gets data of examination.
     * @return current data of examination.
     */
    public Date getDateTaken() {
        return dateTaken;
    }

    /**
     * Searching for students answers.
     * @return current student answer.
     */
    public Hashtable<Integer, Integer> getAnswers() {
        if (this.answers == null) {
            this.answers = new Hashtable<>();
        }
        return answers;
    }

    /**
     * Give a new examination for student.
     * @param examSubject  current examination list of question.
     * @param dateTaken current date of examination.
     */
    public ExamAssignment(ExamSubject examSubject, Date dateTaken) {
        this.examSubject = examSubject;
        this.dateTaken = dateTaken;
    }

    /**
     * Store answer given to the question
     * @param questionId question id.
     * @param answerId answer id.
     */
    public void addAnswer(int questionId, int answerId) {
        this.getAnswers().put(questionId, answerId);
    }

}
