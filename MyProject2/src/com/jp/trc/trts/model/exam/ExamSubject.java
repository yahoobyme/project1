package com.jp.trc.trts.model.exam;

/**
 * List of exam subjects that exist in the system.
 */
public enum ExamSubject {
    Math,
    Physics,
    Biology
}
