package com.jp.trc.trts.model.exam;

import com.jp.trc.trts.model.user.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class creates and fill an array of users and tests.
 * Creates and fills thematic substrings lists of questions.
 */
public class Database {

    private List<ExamQuestion> question = new ArrayList<>();
    private List<ExamAnswer> examAnswer = new ArrayList<>();
    private List<User> user = new ArrayList<>();
    private List<Exam> examList = new ArrayList<>();
    private List<ExamQuestion> questionBio;
    private List<ExamQuestion> questionMath;
    private List<ExamQuestion> questionPhysic;
    private List<Student> student = new ArrayList<>();
    private List<StudentGroup> studentGroup = new ArrayList<>();
    private List<ExamAnswer> examAnswerMath;
    private List<ExamAnswer> examAnswerBio;
    private List<ExamAnswer> examAnswerPhysic;

    /**
     * Gets questions.
     *
     * @return current questions.
     */
    public List<ExamQuestion> getQuestion() {
        return question;
    }

    /**
     * Gets user.
     *
     * @return current user.
     */
    public List<User> getUser() {
        return user;
    }

    /**
     * Gets list of exam.
     *
     * @return current list of exams.
     */
    public List<Exam> getExamList() {
        return examList;
    }

    /**
     * Fills database of users, exam questions, exam questions subject and
     * exam data lists.
     */
    public void init() {
        fillTestQuestion();
        fillExamAnswer();
        initQuestionsBySubject();
        fillTestData();
        fillStudentExamTakes();
        fillStudentGroups();
        fillUsers();
        initAnswersBySubject();

    }

    /**
     * This method adds users into database arrays.
     */
    private void fillUsers() {

        this.user.add(new Teacher("teacherMath", examList));
        this.user.add(new Teacher("teacherPhysic", examList));
        this.user.add(new Teacher("teacherBio", examList));

        Student student = new Student("Ivanov");
        this.user.add(student);
        this.student.add(student);
        this.studentGroup.get(0).addStudent(student);

        student = new Student("Petrov");
        this.user.add(student);
        this.student.add(student);
        this.studentGroup.get(0).addStudent(student);

        student = new Student("Sidorov");
        this.user.add(student);
        this.student.add(student);
        this.studentGroup.get(0).addStudent(student);

        student = new Student("Smirnov");
        this.user.add(student);
        this.student.add(student);
        this.studentGroup.get(1).addStudent(student);

        student = new Student("Skvortsov");
        this.user.add(student);
        this.student.add(student);
        this.studentGroup.get(1).addStudent(student);

        student = new Student("Golovachev");
        this.user.add(student);
        this.student.add(student);
        this.studentGroup.get(1).addStudent(student);

        this.user.add(new Admin("admin1"));
        this.user.add(new Admin("admin2"));
        this.user.add(new Admin("admin3"));
    }

    /**
     * Fill the groups of students.
     */
    private void fillStudentGroups() {
        this.studentGroup.add(new StudentGroup("GroupOne"));
        this.studentGroup.add(new StudentGroup("GroupTwo"));
    }

    private void fillTestQuestion() {

        this.question.add(new ExamQuestion(0, "Среднее арифметическое чисел у; 2,1; 3 и 2,1 равно 2,3. Найдите у: "));

        this.question.add(new ExamQuestion(1, "Какое из следующих чисел делится с остатком на 36?: "));

        this.question.add(new ExamQuestion(2, "Найдите сумму всех целых решений неравенства: (x - 4) / (2x + 6) ≤ 0: "));

        this.question.add(new ExamQuestion(3, "Обратите периодическую дробь 0,2(6) в обыкновенную: "));

        this.question.add(new ExamQuestion(4, "Найдите общий вид первообразной для функции 2sinЗх: "));

        this.question.add(new ExamQuestion(5, "Упростите: (sin4a - sin6a) : (cos5a*sin a): "));

        this.question.add(new ExamQuestion(6, "Число 8 составляет 30% числа b. Сколько процентов числа b + 8 составляет число b?: "));

        this.question.add(new ExamQuestion(7, "Сколько сторон у выпуклого многоугольника, каждый из внутренних углов которого равен 135°?: "));

        this.question.add(new ExamQuestion(8, "Стороны прямоугольника равны 72 м и 32 м. Определите сторону равновеликого ему квадрата: "));

        this.question.add(new ExamQuestion(9, "Сколько всего дробей со знаменателем 36, которые больше 2/3 и меньше 5/6?: "));

        this.question.add(new ExamQuestion(10, "___ - самая распространенная молекула в клетках живых организмов: "));

        this.question.add(new ExamQuestion(11, "___ - идеальный растворитель в живой клетке: "));

        this.question.add(new ExamQuestion(12, "«Рабочей лошадкой» генных инженеров стала: "));

        this.question.add(new ExamQuestion(13, "Автотрофные организмы - это те, которые: "));

        this.question.add(new ExamQuestion(14, "АТФ - это: "));

        this.question.add(new ExamQuestion(15, "Болезнь несвертывания крови называется: "));

        this.question.add(new ExamQuestion(16, "В состав гормонов щитовидной железы входит: "));

        this.question.add(new ExamQuestion(17, "Плацента - это: "));

        this.question.add(new ExamQuestion(18, "Популяция - структурная единица: "));

        this.question.add(new ExamQuestion(19, "При помощи корневой системы растения поглощают из почвы: "));

        this.question.add(new ExamQuestion(20, "Действие электрического тока на магнитную стрелку впервые было обнаружено "));

        this.question.add(new ExamQuestion(21, "Диапазон длин волн 390 нм - 10нм занимает "));

        this.question.add(new ExamQuestion(22, "Единицей магнитного потока является "));

        this.question.add(new ExamQuestion(23, "Если источник света приблизить к преграде, то размер тени "));

        this.question.add(new ExamQuestion(24, "Порядковый номер химического элемента в Периодической системе элементов Д.И.:" +
                " Менделеева называется ___ числом "));

        this.question.add(new ExamQuestion(25, "Число колебаний в единицу времен называется___ колебаний: "));

        this.question.add(new ExamQuestion(26, "Максимальная кинетическая энергия фотоэлектрона зависит от: "));

        this.question.add(new ExamQuestion(27, "Между протонами в ядре действуют силы "));

        this.question.add(new ExamQuestion(28, "Поток магнитной индукции Ф, пронизывающий проволочную рамку, зависит от: "));

        this.question.add(new ExamQuestion(29, "Преобразование энергии электростатического " +
                "поля в электромагнитную энергию происходит в устройстве:"));
    }

    private void fillExamAnswer() {
        this.examAnswer.add(new ExamAnswer(0, "2,6", 0, true));
        this.examAnswer.add(new ExamAnswer(1, "2,1", 0, false));
        this.examAnswer.add(new ExamAnswer(2, "2", 0, false));
        this.examAnswer.add(new ExamAnswer(3, "3,4", 0, false));
        this.examAnswer.add(new ExamAnswer(4, "2648", 1, true));
        this.examAnswer.add(new ExamAnswer(5, "8244", 1, false));
        this.examAnswer.add(new ExamAnswer(6, "3924", 1, false));
        this.examAnswer.add(new ExamAnswer(7, "2016", 1, false));
        this.examAnswer.add(new ExamAnswer(8, "7", 2, true));
        this.examAnswer.add(new ExamAnswer(9, "6", 2, false));
        this.examAnswer.add(new ExamAnswer(10, "8", 2, false));
        this.examAnswer.add(new ExamAnswer(11, "5", 2, false));
        this.examAnswer.add(new ExamAnswer(12, "4/15", 3, true));
        this.examAnswer.add(new ExamAnswer(13, "7/30", 3, false));
        this.examAnswer.add(new ExamAnswer(14, "2/7", 3, false));
        this.examAnswer.add(new ExamAnswer(15, "3/8", 3, false));
        this.examAnswer.add(new ExamAnswer(16, "-2/3 cos3х + С", 4, true));
        this.examAnswer.add(new ExamAnswer(17, "-3/2 sin2х + С", 4, false));
        this.examAnswer.add(new ExamAnswer(18, "2/3 cos3х + С", 4, false));
        this.examAnswer.add(new ExamAnswer(19, "3/2 sin2х + С", 4, false));
        this.examAnswer.add(new ExamAnswer(20, "-2", 5, true));
        this.examAnswer.add(new ExamAnswer(21, "2sina", 5, false));
        this.examAnswer.add(new ExamAnswer(22, "-2cosa", 5, false));
        this.examAnswer.add(new ExamAnswer(23, "-2sina", 5, false));
        this.examAnswer.add(new ExamAnswer(24, "1000/13", 6, true));
        this.examAnswer.add(new ExamAnswer(25, "830/11", 6, false));
        this.examAnswer.add(new ExamAnswer(26, "1300/17", 6, false));
        this.examAnswer.add(new ExamAnswer(27, "1307/17", 6, false));
        this.examAnswer.add(new ExamAnswer(28, "8", 7, true));
        this.examAnswer.add(new ExamAnswer(29, "6", 7, false));
        this.examAnswer.add(new ExamAnswer(30, "5", 7, false));
        this.examAnswer.add(new ExamAnswer(31, "10", 7, false));
        this.examAnswer.add(new ExamAnswer(32, "48", 8, true));
        this.examAnswer.add(new ExamAnswer(33, "36", 8, false));
        this.examAnswer.add(new ExamAnswer(34, "24", 8, false));
        this.examAnswer.add(new ExamAnswer(35, "28", 8, false));
        this.examAnswer.add(new ExamAnswer(36, "5", 9, true));
        this.examAnswer.add(new ExamAnswer(37, "1", 9, false));
        this.examAnswer.add(new ExamAnswer(38, "2", 9, false));
        this.examAnswer.add(new ExamAnswer(39, "3", 9, false));
        this.examAnswer.add(new ExamAnswer(40, "вода", 10, true));
        this.examAnswer.add(new ExamAnswer(41, "белок", 10, false));
        this.examAnswer.add(new ExamAnswer(42, "липиды", 10, false));
        this.examAnswer.add(new ExamAnswer(43, "гликоген", 10, false));
        this.examAnswer.add(new ExamAnswer(44, "вода", 11, true));
        this.examAnswer.add(new ExamAnswer(45, "водород", 11, false));
        this.examAnswer.add(new ExamAnswer(46, "кислород", 11, false));
        this.examAnswer.add(new ExamAnswer(47, "спирт", 11, false));
        this.examAnswer.add(new ExamAnswer(48, "кишечная палочка", 12, true));
        this.examAnswer.add(new ExamAnswer(49, "вирусная частица", 12, false));
        this.examAnswer.add(new ExamAnswer(50, "плазмида", 12, false));
        this.examAnswer.add(new ExamAnswer(51, "дроздофила", 12, false));
        this.examAnswer.add(new ExamAnswer(52, "способны синтезировать органические вещества из неорганических", 13, true));
        this.examAnswer.add(new ExamAnswer(53, "питаются живыми организмами", 13, false));
        this.examAnswer.add(new ExamAnswer(54, "могут синтезировать органические вещества из органических", 13, false));
        this.examAnswer.add(new ExamAnswer(55, "не нуждаются в источнике питания", 13, false));
        this.examAnswer.add(new ExamAnswer(56, "аминокислота", 14, true));
        this.examAnswer.add(new ExamAnswer(57, "белок", 14, false));
        this.examAnswer.add(new ExamAnswer(58, "нуклеотид", 14, false));
        this.examAnswer.add(new ExamAnswer(59, "углевод", 14, false));
        this.examAnswer.add(new ExamAnswer(60, "гемофилией", 15, true));
        this.examAnswer.add(new ExamAnswer(61, "ишемией", 15, false));
        this.examAnswer.add(new ExamAnswer(62, "дальтонизмом", 15, false));
        this.examAnswer.add(new ExamAnswer(63, "тромбофлебитом", 15, false));
        this.examAnswer.add(new ExamAnswer(64, "йод", 16, true));
        this.examAnswer.add(new ExamAnswer(65, "железо", 16, false));
        this.examAnswer.add(new ExamAnswer(66, "хром", 16, false));
        this.examAnswer.add(new ExamAnswer(67, "цинк", 16, false));
        this.examAnswer.add(new ExamAnswer(68, "орган, обеспечивающий условия для развития плода", 17, true));
        this.examAnswer.add(new ExamAnswer(69, "зародыш", 17, false));
        this.examAnswer.add(new ExamAnswer(70, "оплодотворенная яйцеклетка", 17, false));
        this.examAnswer.add(new ExamAnswer(71, "амниотическая жидкость", 17, false));
        this.examAnswer.add(new ExamAnswer(72, "вида", 18, true));
        this.examAnswer.add(new ExamAnswer(73, "отряда", 18, false));
        this.examAnswer.add(new ExamAnswer(74, "биоценоза", 18, false));
        this.examAnswer.add(new ExamAnswer(75, "биосферы", 18, false));
        this.examAnswer.add(new ExamAnswer(76, "воду и минеральные вещества", 19, true));
        this.examAnswer.add(new ExamAnswer(77, "органические вещества", 19, false));
        this.examAnswer.add(new ExamAnswer(78, "минеральные вещества", 19, false));
        this.examAnswer.add(new ExamAnswer(79, "воду", 19, false));
        this.examAnswer.add(new ExamAnswer(80, "Х. Лоренцем", 20, true));
        this.examAnswer.add(new ExamAnswer(81, "П. Ланжевеном", 20, false));
        this.examAnswer.add(new ExamAnswer(82, "А.Ф. Иоффе", 20, false));
        this.examAnswer.add(new ExamAnswer(83, "Х.К. Эрстедом", 20, false));
        this.examAnswer.add(new ExamAnswer(84, "видимый свет", 21, true));
        this.examAnswer.add(new ExamAnswer(85, "инфракрасное излучение", 21, false));
        this.examAnswer.add(new ExamAnswer(86, "рентгеновское излучение", 21, false));
        this.examAnswer.add(new ExamAnswer(87, "ультрафиолетовое излучение", 21, false));
        this.examAnswer.add(new ExamAnswer(88, "1 Вб", 22, true));
        this.examAnswer.add(new ExamAnswer(89, "1 Тл", 22, false));
        this.examAnswer.add(new ExamAnswer(90, "1 А", 22, false));
        this.examAnswer.add(new ExamAnswer(91, "1 В", 22, false));
        this.examAnswer.add(new ExamAnswer(92, "увеличится", 23, true));
        this.examAnswer.add(new ExamAnswer(93, "уменьшится", 23, false));
        this.examAnswer.add(new ExamAnswer(94, "тень исчезнет", 23, false));
        this.examAnswer.add(new ExamAnswer(95, "не изменится", 23, false));
        this.examAnswer.add(new ExamAnswer(96, "зарядовым", 24, true));
        this.examAnswer.add(new ExamAnswer(97, "массовым", 24, false));
        this.examAnswer.add(new ExamAnswer(98, "нуклонным", 24, false));
        this.examAnswer.add(new ExamAnswer(99, "химическим", 24, false));
        this.examAnswer.add(new ExamAnswer(100, "частотой", 25, true));
        this.examAnswer.add(new ExamAnswer(101, "амплитудой", 25, false));
        this.examAnswer.add(new ExamAnswer(102, "периодом", 25, false));
        this.examAnswer.add(new ExamAnswer(103, "фазой", 25, false));
        this.examAnswer.add(new ExamAnswer(104, "частоты света", 26, true));
        this.examAnswer.add(new ExamAnswer(105, "мощности светового потока", 26, false));
        this.examAnswer.add(new ExamAnswer(106, "работы выхода", 26, false));
        this.examAnswer.add(new ExamAnswer(107, "длины световой волны", 26, false));
        this.examAnswer.add(new ExamAnswer(108, "электростатические", 27, true));
        this.examAnswer.add(new ExamAnswer(109, "ядерные", 27, false));
        this.examAnswer.add(new ExamAnswer(110, "магнитные", 27, false));
        this.examAnswer.add(new ExamAnswer(111, "гравитационные", 27, false));
        this.examAnswer.add(new ExamAnswer(112, "косинуса угла поворота рамки", 28, true));
        this.examAnswer.add(new ExamAnswer(113, "сопротивления проволоки", 28, false));
        this.examAnswer.add(new ExamAnswer(114, "площади рамки", 28, false));
        this.examAnswer.add(new ExamAnswer(115, "индукции магнитного поля", 28, false));
        this.examAnswer.add(new ExamAnswer(116, "электрогенератор", 29, true));
        this.examAnswer.add(new ExamAnswer(117, "электрическая батарейка", 29, false));
        this.examAnswer.add(new ExamAnswer(118, "конденсатор", 29, false));
        this.examAnswer.add(new ExamAnswer(119, "элемент солнечных батарей", 29, false));


    }

    private void initQuestionsBySubject() {
        this.questionBio = this.question.subList(10, 20);
        this.questionMath = this.question.subList(0, 10);
        this.questionPhysic = this.question.subList(20, 30);
    }

    private void fillTestData() {
        this.examList.add(new Exam("Физика", ExamSubject.Physics, 0, this.questionPhysic,this.examAnswerPhysic));
        this.examList.add(new Exam("Биология", ExamSubject.Biology, 0, this.questionBio,this.examAnswerBio));
        this.examList.add(new Exam("Математика", ExamSubject.Math, 0, this.questionMath,this.examAnswerMath));
    }

    private void initAnswersBySubject() {
        this.examAnswerBio = this.examAnswer.subList(40, 79);
        this.examAnswerMath = this.examAnswer.subList(0, 39);
        this.examAnswerPhysic = this.examAnswer.subList(80, 119);


    }

    private void fillStudentExamTakes() {
        Random rand = new Random();
        Date examDate = new Date(2020, 4, 4);
        // Loop through students
        this.user.forEach(s -> {
            if (s instanceof Student) {
                // Loop through test subjects
                this.examList.forEach(x -> {
                    ExamAssignment examAssignment = new ExamAssignment(x.getExamSubject(), examDate);
                    // Loop through test questions and create exam take with random answers (not all will be correct)
                    x.getQuestion().forEach(y -> examAssignment.addAnswer(y.getQuestionId(), rand.nextInt(y.getAnswer().size)));
                    ((Student) s).addExamResult(examAssignment);
                });
            }
            ;
        });
    }
}