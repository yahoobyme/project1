package com.jp.trc.trts.model.exam;

import java.util.List;

/**
 * This class create exam of exam system.
 * Tests contains questions.
 */
public class Exam {
    private  String examId;
    private ExamSubject examSubject;
    private  List<ExamQuestion> question;
    private List<ExamAnswer> answer;
    private  int examResult;

    /**
     * Gets exam result
     * @return current exam result.
     */
    public int getExamResult() {
        return examResult;
    }

    /**
     * Gets test ID.
     * @return current exam ID.
     */
    public String getExamId() {
        return examId;
    }

    /**
     * Gets list of exam question.
     * @return current list of exam question .
     */
    public List<ExamQuestion> getQuestion() {
        return question;
    }

    /**
     * Gets test subject.
     * @return current exam subject.
     */
    public ExamSubject getExamSubject() {
        return examSubject;
    }

    /**
     * Creates object of test.
     * @param examId ID of test.
     * @param examSubject subject.
     * @param question question of test.
     */
    public Exam(String examId, ExamSubject examSubject, int examResult, List<ExamQuestion> question, List<ExamAnswer> answer) {
        this.examId = examId;
        this.examSubject = examSubject;
        this.examResult = examResult;
        this.question = question;
        this.answer = answer;
    }

}

