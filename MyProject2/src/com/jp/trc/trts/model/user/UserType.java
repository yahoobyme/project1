package com.jp.trc.trts.model.user;

/**
 * List of user type  that exist in the system.
 */

public enum UserType {
    Teacher,
    Student,
    Admin
}



