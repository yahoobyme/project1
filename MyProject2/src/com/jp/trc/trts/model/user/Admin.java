package com.jp.trc.trts.model.user;

/**
 * This class create users of Administrator user group.
 */
public class Admin extends User {
    /**
     * Creates user of admin user group.
     * @param userName name of admin user.
     */
    public Admin(String userName) {
        super(userName, UserType.Student);
    }
}

