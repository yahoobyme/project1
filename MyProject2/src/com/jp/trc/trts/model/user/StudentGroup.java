package com.jp.trc.trts.model.user;


import java.util.ArrayList;
import java.util.List;

/**
 * There is class of students groups.
 */
public class StudentGroup {
    private String groupName;
    private List<Student> students;

    /**
     * Creates groups of students.
     *
     * @param groupName name of current group.
     * @param students  list of students in the group.
     */
    public StudentGroup(String groupName, List<Student> students) {
        this.groupName = groupName;
        this.students = students;
    }

    /**
     * Creates student group arrays.
     * @param groupName current group name.
     */
    public StudentGroup(String groupName) {
        this.groupName = groupName;
        this.students = new ArrayList();
    }

    /**
     * Adds current student to group array list.
     * @param student current student adds.
     */
    public void addStudent(Student student) {
        this.students.add(student);
    }
}