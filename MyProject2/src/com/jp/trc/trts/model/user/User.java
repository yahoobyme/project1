package com.jp.trc.trts.model.user;

/**
 * Create users of the test system. Users are divided into groups
 * with different user access and console interface.
 * This is class parent for all classes of user group.
 */
public class User {

    private  String userName;
    private  UserType userType;
    /**
     * Gets user name.
     * @return current user name.
     */
    public String getUserName() {
        return userName;
    }
    /**
     * Gets user group.
     * @return current user type.
     */
    public UserType getUserType() {
        return userType;
    }

    /**
     * Creates object of user class.
     * @param userName name of user.
     * @param userType user group ID
     */
    public User(String userName, UserType userType) {
        this.userName = userName;
        this.userType = userType;
    }
}


