package com.jp.trc.trts.model.user;

import com.jp.trc.trts.model.exam.ExamAssignment;

import java.util.ArrayList;
import java.util.List;

/**
 * This class create users of Student user group.
 * Student also can belong to student group which is different from user group.
 */
public class Student extends User {



    private List<ExamAssignment> examResults;


    /**
     * There is creates students users.

     */
    public Student(String userName) {
        super(userName, UserType.Student);

    }

    /**
     * Gets exam results.
     *
     * @return current exam results.
     */
    public List<ExamAssignment> getExamResults() {
        return examResults;
    }

    /**
     * Adds results of student examination.
     *
     * @param examAssignment current examination.
     */
    public void addExamResult(ExamAssignment examAssignment) {
        if (this.examResults == null) {
            this.examResults = new ArrayList<>();
        }
        this.examResults.add(examAssignment);
    }
}

