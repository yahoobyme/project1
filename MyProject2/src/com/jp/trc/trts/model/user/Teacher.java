package com.jp.trc.trts.model.user;

import com.jp.trc.trts.model.exam.Exam;

import java.util.List;

/**
 * This class create users of Teacher user group.
 */
public class Teacher extends User {


    private List<Exam> courses;


    /**
     * Gets teacher course name.
     * @return current course name.
     */
    public List<Exam> getCourses() {
        return courses;
    }

    /**
     * Creates object of teacher class.
     * @param userName name of teacher.
     * @param courses course of teacher.
     */
    public Teacher(String userName, List<Exam> courses) {
        super(userName, UserType.Teacher);
        this.courses = courses;
    }
}

