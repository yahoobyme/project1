package com.jp.trc.trts.view;

import java.util.Scanner;

/**
 * This class handles input from keyboard.
 */
public class InputService {
    /**
     * Waiting for input string types from keyboard.
     * @return string type.
     */
    public String waitForInputString() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
    /**
     * Waiting for input byte types from keyboard.
     * @return byte type.
     */
    public byte waitForInputByte() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextByte();
    }
}
