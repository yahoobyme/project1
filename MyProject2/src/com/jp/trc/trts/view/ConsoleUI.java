package com.jp.trc.trts.view;

import com.jp.trc.trts.controller.UserController;
import com.jp.trc.trts.model.user.UserType;


/**
 * This class adds console interface for users whichever group of users logging in
 */
public class ConsoleUI {

    private UserController userController;
    private OutputService outputService;
    private InputService inputService;

    public ConsoleUI(UserController userController, OutputService outputService, InputService inputService) {
        this.userController = userController;
        this.outputService = outputService;
        this.inputService = inputService;
    }

    /**
     * Runs user console interface.
     */
    public void run() {
        outputService.outputMessage("Введите имя пользователя: ");
        /**
         * User enter user name for login.
         */
        String currentUserName = inputService.waitForInputString();
        UserType userType = this.userController.searchUserGroupByUserName(currentUserName);

        /**
         * Start console interface witch current user group.
         */
        switch (userType) {
            case Teacher -> this.viewTeacherUI();
            case Student -> this.viewStudentUI();
            case Admin -> this.viewAdminUI();
            default -> outputService.outputMessage("Меню недоступно! ");
        }
    }

    /**
     * This method creates console interface which opens access for students user group
     * Students can view test name and how many test questions from test array.
     */
    public void viewStudentUI() {
        outputService.outputMessage("Меню выбора теста. ");
        outputService.outputMessage("Выбирите нужный тест: ");
        for (int i = 0; i < this.userController.getTests().size(); i++) {
            outputService.outputMessage(i + ". " + this.userController.getTests().get(i).getExamId());
        }
        byte choice = inputService.waitForInputByte();
        outputService.outputMessage(choice + ". " + this.userController.getTests().get(choice).getExamId() + ":" + "\n" + "    Количество вопросов: "
                + this.userController.getTests().get(choice).getQuestion().size());
    }


    /**
     * This method creates console interface which opens access for teachers user group.
     * Teachers can view tests and tests result from test array.
     */
    public void viewTeacherUI() {
        outputService.outputMessage("Вы в меню преподавателя. " + "\n" + "Выберите тест для просмотра успеваемости: ");


        for (int i = 0; i < this.userController.getTests().size(); i++) {
            outputService.outputMessage(i +". " + this.userController.getTests().get(i).getExamId());
        }


        byte choice = inputService.waitForInputByte();
        outputService.outputMessage(this.userController.getTests().get(choice).getExamId() + " Успеваемость: " + this.userController.getTests().get(choice).getExamResult());

        outputService.outputMessage("Выберите тест для просмотра заданий");
        for (int i = 0; i < this.userController.getTests().size(); i++) {
            outputService.outputMessage(i + ". " + this.userController.getTests().get(i).getExamId());
        }
        choice = inputService.waitForInputByte();

        for (int i = 0; i < this.userController.getTests().get(choice).getQuestion().size(); i++) {
            outputService.outputMessage(i + ". " + this.userController.getTests().get(choice).getQuestion().get(i).getQuestionText() + "\n" + "  Ответ: "
                    + this.userController.getTests().get(choice).getQuestion().get(i).getCorrectAnswerText());
        }

    }

    /**
     * This method creates console interface which opens access for administrator user group.
     * Administrators can view user group arrays here.
     */
    public void viewAdminUI() {
        outputService.outputMessage("Вы в меню администратора. " + "\n" + "Список пользователей: ");
        for (int i = 0; i < this.userController.getUser().size(); i++) {
            outputService.outputMessage(i + ". " + "Пользователь: " + this.userController.getUser().get(i).getUserName() + "." +
                    " Группа - " + this.userController.getUser().get(i).getUserType());
        }

    }

}
