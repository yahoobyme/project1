package com.jp.trc.trts.view;

/**
 * This class give output messages.
 */
public class OutputService {
    /**
     * There is processing system output messages.
     * @param message current output message.
     */
    public void outputMessage(String message) {
        System.out.println(message);
    }
}
